/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.nannapat.finaltest;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class FinalTest implements Serializable{
    private String name;
    private String gender;
    private int age;
    private int money;
    private String date;
    private String time; 

    @Override
    public String toString() {
        return "Borrower{" + "name=" + name + ", gender=" + gender + ", age=" + age + ", money=" + money + ", date=" + date + ", time=" + time + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public FinalTest(String name, String gender, int age, int money, String date, String time) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.money = money;
        this.date = date;
        this.time = time;
    }
}
